'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

  resolve: {
   modules: ['/usr/share/nodejs'],
  },

  resolveLoader: {
   modules: ['/usr/share/nodejs'],
  }
}

module.exports = config;
