This is a stub types definition for vue-resource (https://github.com/vuejs/vue-resource).

vue-resource provides its own type definitions, so you don't need @types/vue-resource installed!